# -*- coding: utf-8 -*-
import sys

# #noinspection PyBroadException
# try:
#     sys.path.append("C:\\Program Files\\Tesseract-OCR")
# except Exception:
#     pass
import os
import re
import time

from cv2 import cv2

import numpy as np
import pytesseract

__author__ = 'ozgur'
__creation_date__ = '18.05.2019' '16:06'

# pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe'
pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'


class Conf:
    def __init__(self):
        self.cid = 0  # camera id (birden fazla kamera varsa seçmek için)
        self.cwidth = 0  # belirli bir çözünürlüğe zorlamak için kullanılır
        self.cheight = 0  # belirli bir çözünürlüğe zorlamak için kullanılır
        self.opath = "out.txt"  # varsayılan out.txt


class IdReader:

    def __init__(self):
        self.conf = Conf()
        self.idlist = []
        self._read_id_list()

    def _read_id_list(self):
        with open(self.conf.opath, "w+") as ofile:
            for line in ofile.read().split("\n"):
                # noinspection PyBroadException
                try:
                    self.idlist.append(int(line.strip()))
                except Exception:
                    pass

    def add_id(self, tcid: int):
        if tcid in self.idlist:
            pass
        else:
            self.idlist.append(tcid)
            with open(self.conf.opath, "a+") as ofile:
                ofile.write("{}\n".format(tcid))

    def processimage(self, cv2image):
        """
        Resim temizleme , text ayiklama ve db yazma burada yapiliyor
        :param cv2image: cv2 ile okunmus video frame veya image dosyasi
        :return: None
        """

        # Resmi rescale ediyoruz
        img = cv2.resize(cv2image, None, fx=1.5, fy=1.5, interpolation=cv2.INTER_CUBIC)

        # noise azaltıyoruz
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        kernel = np.ones((1, 1), np.uint8)
        img = cv2.dilate(img, kernel, iterations=1)
        img = cv2.erode(img, kernel, iterations=1)

        # netlestirmek icin blur uyguluyoruz
        img = cv2.GaussianBlur(img, (5, 5), 0)

        # binaryzation, siyah beyaz hale getiriyoruz
        img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]

        # kalani tesseract hallediyor
        rettext = pytesseract.image_to_string(img, lang="eng")

        # 11 haneli TC kimlik no ayikliyoruz
        tcidlist = re.findall(r"\D(\d{11})\D", rettext)
        if tcidlist:
            print("Okundu : ", tcidlist[0])
            self.add_id(int(tcidlist[0]))

    def readimage(self, imgpath):
        """
        Verilen yoldaki img dosyasini okuyup isler
        :param imgpath: os.path : file
        :return: None
        """
        try:
            img = cv2.imread(imgpath)
            self.processimage(img)
        except Exception as ex:
            print("Resmi okurken hata: '{}' -> {}".format(imgpath,str(ex)))
            return None

    def readimagefolder(self, imgfolderpath):
        """

        Verilen yoldaki img dosyalarini okuyup isler
        :param imgfolderpath: os.path : folder
        :return: None
        """
        for root, dirs, files in os.walk(imgfolderpath):
            for filename in files:
                filepath = os.path.join(root, filename)

                self.readimage(filepath)

    def readcamera(self):
        """
        settings dosyasinda belitilen kamera aygitini okur, 'q' ile cikis yapar
        :return: None
        """
        cap = cv2.VideoCapture(self.conf.cid)
        time.sleep(1)

        if self.conf.cwidth and self.conf.cheight:
            cap.set(cv2.CAP_PROP_FRAME_WIDTH, self.conf.cwidth)
            cap.set(cv2.CAP_PROP_FRAME_HEIGHT, self.conf.cheight)
            time.sleep(1)
        else:
            pass
        print("Kamera cozunurlugu: (" + str(cap.get(cv2.CAP_PROP_FRAME_WIDTH)) + "; " + str(
            cap.get(cv2.CAP_PROP_FRAME_HEIGHT)) + ")")

        if not cap.isOpened():
            print("Kamera acilamadi!")

        while cap.isOpened():
            ret, frame = cap.read()
            if ret:
                cv2.imshow('Frame', frame)
                self.processimage(frame)
                # Q geldiyse cikis yapar
                if cv2.waitKey(25) & 0xFF == ord('q'):
                    break
            else:
                break
        cap.release()
        cv2.destroyAllWindows()


if __name__ == '__main__':
    IdReader().readcamera()

    # # Test icin testimages dizininne resimler ekleyip sonuçları izleyebilirsiniz
    # IdReader().readimagefolder("testimages")


